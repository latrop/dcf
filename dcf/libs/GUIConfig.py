#! /usr/bin/env python

from collections import OrderedDict
from types import SimpleNamespace
from os import path
import tkinter as tk
import shlex
import shutil


class ConfigWindow(tk.Frame):
    """ Window to manage some secondary configuration parameters """
    def __init__(self, window):
        self.window = window
        self.top = tk.Toplevel(window.root)
        rootX = self.window.root.winfo_x()
        rootY = self.window.root.winfo_y()
        self.top.attributes('-topmost', 'true')
        self.top.geometry("+%d+%d" % (rootX + 30, rootY + 20))

        num_of_params = len(self.window.params)
        # Create entries based on the config file content. For every entry in the config file
        # there will be one entry created in the current window
        self.tk_variables = [tk.StringVar() for _ in range(num_of_params)]
        for idx, (name, par) in enumerate(self.window.params.items()):
            value = par.value
            self.tk_variables[idx].set(str(value))
            tk.Label(self.top, text=name).grid(column=0, row=idx)
            tk.Entry(self.top, textvariable=self.tk_variables[idx], width=6).grid(column=1, row=idx)

        # Save button
        tk.Button(self.top, text="Apply & save", command=self.accept_and_save).grid(column=0, row=num_of_params+1)

        # Accept button
        tk.Button(self.top, text="Apply", command=self.accept).grid(column=1, row=num_of_params+1)

    def accept(self):
        """ Only set new values of config parameters locally. Do not overwrite the config file. """
        for idx, var in enumerate(self.tk_variables):
            new_value = var.get()
            name = list(self.window.params.keys())[idx]
            self.window.params[name].value = new_value
        self.top.destroy()

    def accept_and_save(self):
        """ Save parameters locally and overwrite the config file """
        config_name = path.join(path.dirname(__file__), "config.dat")
        tmp_config_name = path.join(path.dirname(__file__), "config.tmp")
        for idx, var in enumerate(self.tk_variables):
            new_value = var.get()
            name = list(self.window.params.keys())[idx]
            self.window.params[name].value = new_value
        tmp_config_file = open(tmp_config_name, "w")
        tmp_config_file.truncate(0)
        # Copy header in the current config file
        for line in open(config_name):
            if line.strip().startswith("#"):
                tmp_config_file.write(line)
        # Write down parameters
        for name, par in self.window.params.items():
            out_line = '"%s"       %s   # %s\n' % (name, par.value, par.comment)
            tmp_config_file.write(out_line)
        tmp_config_file.close()

        shutil.move(tmp_config_name, config_name)
        self.top.destroy()


def parse_config_file(path_to_config):
    params = OrderedDict()
    for line in open(path_to_config):
        if line.startswith("#"):
            continue
        name_and_value, comment = line.split("#")
        comment = comment.strip()
        name, value = shlex.split(name_and_value)
        params[name] = SimpleNamespace(name=name, value=value, comment=comment)
    return params
