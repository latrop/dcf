#! /usr/bin/env python

import datetime

import os
from os import path

from numpy import argsort
from numpy import array
from numpy import genfromtxt
from numpy import savetxt
from numpy import zeros_like
from numpy import where
from numpy import cumsum
from numpy import searchsorted
from numpy import arange
from numpy import mean
from numpy import linspace
from numpy import log10
from numpy import exp
from numpy.random import choice, random, normal
from numpy import ediff1d
from numpy import polyfit
from numpy import poly1d
from scipy.stats import gaussian_kde
from scipy.interpolate import interp1d
from scipy.integrate import quad


from matplotlib import pyplot


class TimeSeries(object):
    """ Class represents a time series of some
    observable, e.g. light curve"""

    def __init__(self, times, values, sigmas=None, name="noname", default_sigma_value=0.02):
        inds = argsort(times)  # make sure all points are sorted
        self.name = name
        self.times = times[inds].copy()
        self.values = values[inds].copy()
        if sigmas is not None:
            self.sigmas = sigmas[inds].copy()
        else:
            self.sigmas = default_sigma_value * self.times
        self.length = len(times)
        # We really want the times to be in MDJ, but sometimes they
        # are provided as JD. So let's check and convert JD->MJD in neccessary
        if self.times[0] > 2400000.5:
            # Definitely JD
            self.times = jd_to_mjd(self.times)
        # Store also backup values to restore them if necessary
        self.origTimes = self.times.copy()
        self.origValues = self.values.copy()
        self.origSigmas = self.sigmas.copy()

    @classmethod
    def from_file(cls, fileName, usecols=None, default_sigma_value=0.02):
        if usecols is None:
            usecols = [1, 2, 3]
        if len(usecols) == 3:
            # All three columns are provided
            times, values, sigmas = genfromtxt(fileName, usecols=usecols, unpack=True)
        elif len(usecols) == 2:
            # Only times and values columns are provided
            times, values = genfromtxt(fileName, usecols=usecols, unpack=True)
            sigmas = default_sigma_value * values
        name = path.splitext(path.basename(fileName))[0]
        ts = cls(times, values, sigmas, name)
        return ts

    def __getitem__(self, value):
        """ Method allows one to extract part of the time series according
        to a given set of indexes"""
        selectedTimes = self.times[value]
        selectedValues = self.values[value]
        selectedSigmas = self.sigmas[value]
        return TimeSeries(selectedTimes, selectedValues, selectedSigmas, name=self.name+"_sub")

    def time_constraint(self, tMin, tMax):
        """ Method allows one to extract a part of the time series according to
        a given time range"""
        inds = where((self.times > tMin) & (self.times < tMax))[0]
        selectedTimes = self.times[inds]
        selectedValues = self.values[inds]
        selectedSigmas = self.sigmas[inds]
        return TimeSeries(selectedTimes, selectedValues, selectedSigmas, name=self.name+"_constr")

    def show_info(self):
        print("----------------------")
        print("Light curve: %s" % self.name)
        print("Num of points: %i" % self.length)
        print("Time range: %1.2f (%1.2f -- %1.2f)" % (self.times[-1] - self.times[0], self.times[0], self.times[-1]))
        print("Mean value: %1.2f" % mean(self.values))
        print("Mean gap: %1.2f" % (mean(ediff1d(self.times))))

    def plot(self):
        pyplot.errorbar(self.times, self.values, yerr=self.sigmas, fmt="ro")
        pyplot.show()
        pyplot.clf()

    def to_png(self, name):
        pyplot.errorbar(self.times, self.values, yerr=self.sigmas)
        pyplot.savefig(name)
        pyplot.clf()

    def to_text(self, fName):
        i = list(range(self.length))
        hdr = "# n  time   value   sigma"
        savetxt(fName, array([i, self.times, self.values, self.sigmas]).T, header=hdr)

    def flux_randomization(self):
        """ Function add gaussian shift to every data point
        according to it's sigma value."""
        noise = normal(zeros_like(self.sigmas),
                       self.sigmas)
        noisedValues = self.values + noise
        return TimeSeries(self.times, noisedValues, self.sigmas)

    def random_subset_selection(self, fraction=0.632):
        """ Function selects random subset of data.
        Input: fraction -- fraction of points to select.
               default = 1-1/e=0.632"""
        selectedInds = sorted(choice(a=range(self.length), size=int(self.length*fraction),
                                     replace=False))
        selectedTimes = self.times[selectedInds]
        selectedValues = self.values[selectedInds]
        selectedSigmas = self.sigmas[selectedInds]
        return TimeSeries(selectedTimes, selectedValues, selectedSigmas)

    def rss_and_fr(self, fraction=0.632):
        """ Function performs both flux randomization and
        random subset selection"""
        selected = self.random_subset_selection(fraction)
        return selected.flux_randomization()

    def copy_time_sampling(self, ts):
        """ Function copies time sampling of the given time series
        to the self one. Time range of slef must overlap the one of
        the given ts.
        Input:
             ts: TimeSeries object to copy it time sampling"""
        idx = where((ts.times > self.times[0]) & (ts.times < self.times[-1]))
        interpFlux = interp1d(self.times, self.values)
        interpSigma = interp1d(self.times, self.sigmas)
        newTimes = ts.times[idx]
        newFlux = interpFlux(newTimes)
        newSigmas = interpSigma(newTimes)
        self.times = newTimes
        self.values = newFlux
        self.sigmas = newSigmas
        self.length = len(self.times)

    def emulate_time_sampling_average(self, ts):
        """ Function emulates the time sampling of a given
        ts and sets it to the self one. To simulate the time sampling
        the function at first estimates the distribution of the time
        bins and then draws random time bins with the obtained PDF"""
        timeBins = ediff1d(ts.times)   # find all bins
        kde = gaussian_kde(timeBins)   # compute the kernel density estimation
        newBins = arange(0.5, max(timeBins), 0.25)
        pdf = kde.evaluate(newBins)  #
        cdf = cumsum(pdf)            # compute pdf and cdf
        cdf /= cdf[-1]               #
        tCurrent = self.times[0]
        emulTimes = [tCurrent]
        while 1:
            # pick random bin and add it to the current time
            binIdx = searchsorted(cdf, random())
            newBin = newBins[binIdx]
            tCurrent += newBin
            if tCurrent >= self.times[-1]:
                # if the current time went further than the rightmost
                # point of the time series, then we are done
                break
            emulTimes.append(tCurrent)

        # interpolate the flux using the new observation times
        valuesInterp = interp1d(self.times, self.values)
        sigmasInterp = interp1d(self.times, self.sigmas)
        emulValues = valuesInterp(emulTimes)
        emulSigmas = sigmasInterp(emulTimes)
        self.times = array(emulTimes)
        self.values = emulValues
        self.sigmas = emulSigmas
        self.length = len(emulTimes)

    def emulate_time_sampling_season(self, ts):
        """ Function emulates the time sampling of a given
        ts, but takes into account seasonal variations. self.times should be
        in MJD for this function to work properly"""
        # Let's find at first the distribution of data points by month of year
        monthlyBins = [[] for _ in range(12)]
        for i in range(len(ts.times[:-1])):
            tmjd = (ts.times[i+1]+ts.times[i]) / 2
            year, month, day = mjd_to_ymd(tmjd)
            bSize = ts.times[i+1] - ts.times[i]
            if bSize < 25:
                monthlyBins[month-1].append(bSize)

        # Compute CDF of bins for every month
        monthlyCDF = []
        monthlyBinsRange = []
        for month in range(12):
            if len(monthlyBins[month]) > 1:
                kde = gaussian_kde(monthlyBins[month])
                binRange = linspace(0, max(monthlyBins[month]), 100)
                monthlyBinsRange.append(binRange)
                pdf = kde.evaluate(binRange)
                cdf = cumsum(pdf)
                cdf /= cdf[-1]
                monthlyCDF.append(cdf)
            else:
                # there is no observation this month -> no CDF computed
                monthlyCDF.append(None)
                monthlyBinsRange.append([])

        # Compute emuolated time bins
        tCurrent = self.times[0]
        emulTimes = [tCurrent]
        while 1:
            # Find the month of the current day
            year, currentMonth, day = mjd_to_ymd(tCurrent)
            cdf = monthlyCDF[currentMonth-1]
            if cdf is not None:
                # Use the CDF of the current month to draw the random bin
                binIdx = searchsorted(cdf, random())
                newBin = monthlyBinsRange[currentMonth-1][binIdx]
                tCurrent += newBin
                if tCurrent >= self.times[-1]:
                    break
                emulTimes.append(tCurrent)
            else:
                # if CDF is None, then there is no observations this month,
                # so we can just move forward a bit untill the month is over
                tCurrent += 1

        # interpolate the flux using the new observation times
        valuesInterp = interp1d(self.times, self.values)
        sigmasInterp = interp1d(self.times, self.sigmas)
        emulValues = valuesInterp(emulTimes)
        emulSigmas = sigmasInterp(emulTimes)
        self.times = array(emulTimes)
        self.values = emulValues
        self.sigmas = emulSigmas
        self.length = len(emulTimes)

    def emulate_time_sampling_flux(self, ts):
        """ Function emulates dependence of the time sampling on the flux value.
        For some light curves (e.g. for gamma light curves with the adaptive
        binning) binning is finer when the flux is high."""
        tCurrent = self.times[0]
        valuesInterp = interp1d(self.times, self.values)
        emulTimes = [tCurrent]
        while 1:
            fluxCurrent = valuesInterp(tCurrent)
            # find observations of ts light curve with similar fluxes
            inds = argsort(abs(ts.values[:-1] - fluxCurrent))[:20]
            # find bins that of these observations
            bins = [ts.times[i+1]-ts.times[i] for i in inds]
            if bins.count(bins[0]) == len(bins):  # all bins are equal
                newBin = bins[0]
            else:
                # create pdf and cdf
                binRange = linspace(0, max(bins), 100)
                kde = gaussian_kde(bins)
                pdf = kde.evaluate(binRange)
                cdf = cumsum(pdf)
                cdf /= cdf[-1]
                # draw random bin
                binIdx = searchsorted(cdf, random())
                newBin = binRange[binIdx]
            tCurrent += newBin
            if tCurrent > self.times[-1]:
                break
            emulTimes.append(tCurrent)
        # interpolate the flux using the new observation times
        valuesInterp = interp1d(self.times, self.values)
        sigmasInterp = interp1d(self.times, self.sigmas)
        emulValues = valuesInterp(emulTimes)
        emulSigmas = sigmasInterp(emulTimes)
        self.times = array(emulTimes)
        self.values = emulValues
        self.sigmas = emulSigmas
        self.length = len(emulTimes)

    def emulate_sigmas(self, ts):
        """ Function sets random sigma values to a current time series
        in such a way that the overall distribution of sigmas on the
        'sigma-flux' diagram copies one of a given ts. On other words,
        it copies the dependence sigma values on flux values."""
        newSigmas = []
        for flux in self.values:
            # find several flux points with close values to a given
            iNearest = abs(ts.values-flux).argsort()[:15]
            # find corresponding sigma values and approximate their distribution
            # with KDE method
            sBin = ts.sigmas[iNearest]
            kde = gaussian_kde(sBin)
            sigmaRanges = linspace(0.75*min(sBin), 1.25*max(sBin), 5*len(sBin))
            pdf = kde.evaluate(sigmaRanges)
            cdf = cumsum(pdf)
            cdf /= cdf[-1]
            # Draw random sigma value with obtained cumulative distribution function
            sigmaValue = sigmaRanges[searchsorted(cdf, random())]
            newSigmas.append(sigmaValue)
        self.sigmas = array(newSigmas)

    def flux_to_mags(self, m0):
        """ Function converts values of time series from flux units
        to magnitudes using m0 as a zero magnitude"""
        self.sigmas = 1.08573 * self.sigmas / self.values
        self.values = -2.5*log10(self.values) + m0

    def mags_to_flux(self, m0):
        """
        Function converts values of time series from magnitude
        units to fluxes using m0 as a zero-point magnitude
        """
        self.sigmas = self.sigmas * 0.92 * exp(0.92*(m0 - self.values))
        self.values = 10 ** (0.4 * (m0 - self.values))

    def logflux(self):
        """ Compute logarithm of a flux """
        self.sigmas = self.sigmas / (2.30*self.values)
        self.values = log10(self.values)

    def detrend(self, degree, plot=False):
        """ Function performs detrending of the light curve by subtracting
        best fit polynomial on a given degree from the data.
        degree: degree of the polynomial to fit
        plot: make a plot to illustrate the detrending results"""

        # Obtain evenly spaced time series to perform polynomial fit on it
        valuesInterp = interp1d(self.times, self.values)
        sigmasInterp = interp1d(self.times, self.sigmas)
        timesNew = linspace(self.times[0], self.times[-1], self.length)
        valuesNew = valuesInterp(timesNew)
        sigmasNew = sigmasInterp(timesNew)
        weights = 1/sigmasNew

        # Fit poly
        coeffs = polyfit(x=timesNew, y=valuesNew, w=weights, deg=degree)
        poly = poly1d(coeffs)
        trend = poly(self.times)
        # Subptract trend
        detrendedValues = self.values - poly(self.times)
        if plot:
            pyplot.subplot(211)
            pyplot.plot(self.times, self.values)
            pyplot.plot(self.times, trend)
            pyplot.subplot(212)
            pyplot.plot(self.times, detrendedValues)
            if not path.exists("outFigs"):
                os.makedirs("outFigs")
            pngName = path.join("outFigs", "%s_detrend.png" % self.name)
            pyplot.savefig(pngName)
            pyplot.clf()
        self.values = detrendedValues.copy()

    def restore(self):
        """ Restore original values """
        self.times = self.origTimes.copy()
        self.values = self.origValues.copy()
        self.sigmas = self.origSigmas.copy()
        self.length = len(self.times)


def convolve_ts_with_func(ts, func):
    """ Function perfroms the convolution of the time series
    with the given function.
         ts: TimeSeries object
         func: callable, must support numpy-like vector computation support"""
    tsInterp = interp1d(ts.times, ts.values, assume_sorted=True, fill_value=0.0, bounds_error=False)
    convolvedValues = zeros_like(ts.values)
    for idx, t in enumerate(ts.times):
        def f(tau):
            return tsInterp(t-tau) * func(tau)
        convolvedValues[idx] = quad(f, -100, 100)[0]  # Fix boundaries
        print(idx)

    return TimeSeries(ts.times, convolvedValues, zeros_like(ts.times)+0.5)


def mjd_to_decimal_years(mjd):
    """
    Function converts given mjd value to decimal gregorian years
    """
    delta = datetime.timedelta(days=mjd)
    mjdStart = datetime.datetime(1858, 11, 17)  # beginning of the MJD epoh
    tCurrent = mjdStart + delta
    currentYear = tCurrent.year
    yearLength = datetime.datetime(currentYear+1, 1, 1) - datetime.datetime(currentYear, 1, 1)
    secondsInYear = yearLength.total_seconds()
    tFromYearStart = tCurrent - datetime.datetime(currentYear, 1, 1)
    secondsFromYearStart = tFromYearStart.total_seconds()
    fracYear = secondsFromYearStart / secondsInYear
    return currentYear + fracYear


def decimal_year_to_mjd(decYear):
    """ Function converts decimal years to MJD data """
    year = int(decYear)
    decPart = decYear - year
    yearBegin = datetime.datetime(year, 1, 1)
    yearLength = datetime.datetime(year+1, 1, 1) - datetime.datetime(year, 1, 1)
    secondsInYear = yearLength.total_seconds()
    currentData = yearBegin + datetime.timedelta(seconds=secondsInYear*decPart)
    timeFromMJDStart = currentData - datetime.datetime(1858, 11, 17)
    return timeFromMJDStart.days + timeFromMJDStart.seconds/86400


def year_by_mjd(mjd):
    """ Function finds current Gregorian year by a given MJD date."""
    delta = datetime.timedelta(days=mjd)
    mjdStart = datetime.datetime(1858, 11, 17)  # beginning of the MJD epoh
    tCurrent = mjdStart + delta
    return tCurrent.year


def mjd_to_ymd(mjd):
    """ Function finds Georgian year, month and day by a given MJD date """
    delta = datetime.timedelta(days=mjd)
    mjdStart = datetime.datetime(1858, 11, 17)  # beginning of the MJD epoh
    tCurrent = mjdStart + delta
    y = tCurrent.year
    m = tCurrent.month
    d = tCurrent.day
    return y, m, d


def jd_to_mjd(jd):
    """ Converts Julian day to modified Julian day """
    mjd = jd - 2400000.5
    return mjd
